import random
import time


bcolors = ['\033[95m', '\033[94m', '\033[96m', '\033[92m', '\033[93m', '\033[91m', '\033[1m', '\033[4m']

def hello(n) :
	c = 0

	for j in range(n) :

		for i in "Hello " :
			c += 1

			char = random.choice([i,i.upper()])
			char = random.choice(bcolors) + char + '\033[0m'
			print(c*' ' , char)
			time.sleep(random.random()/2)

hello(5)